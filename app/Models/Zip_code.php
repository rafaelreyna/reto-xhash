<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Zip_code extends Model
{
    public function settlements() {
        return $this->hasMany('App\Models\Settlement');
    }

    public function municipality() {
        return $this->belongsTo('App\Models\Municipality');
    }

    public function federal_entity() {
        return $this->belongsTo('App\Models\Federal_entity');
    }
}
