<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settlement extends Model
{
    public function zip_code() {
        return $this->belongsTo('App\Models\Zip_code');
    }

    public function settlement_type() {
        return $this->belongsTo('App\Models\Settlement_type');
    }
}
