<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Municipality extends Model
{
    public function federal_entity() {
        return $this->belongsTo('App\Models\Federal_entity');
    }
}
