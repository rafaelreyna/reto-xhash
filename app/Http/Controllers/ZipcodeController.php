<?php

namespace App\Http\Controllers;

use App\Http\Resources\Zip_code as Zip_codeResource;
use App\Models\Zip_code;
use Illuminate\Http\Request;

class ZipcodeController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  string  $code
     * @return Zip_code
     */
    public function show($code)
    {
        $zipCode = Zip_code::where('zip_code','=',$code)->firstOrFail();
        return new Zip_codeResource($zipCode);
    }

}
