<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Zip_code extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'zip_code' => $this->zip_code,
            'locality' => $this->locality,
            'federal_entity' => new Federal_entity($this->federal_entity),
            'settlements' => new SettlementCollection($this->settlements),
            'municipality' => new Municipality($this->municipality),
        ];
    }
}
