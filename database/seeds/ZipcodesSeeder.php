<?php

use Illuminate\Database\Seeder;

class ZipcodesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Extrayendo y parseando la información del XML descargado
        $zipcodesXMLData = file_get_contents(__DIR__.'/CPdescarga.xml');
        @$zipcodesData = new SimpleXMLElement($zipcodesXMLData);

        // Generando un ciclo para cada registro
        foreach ($zipcodesData as $zipcodeData) {

            //verificar si el zip_code existe
            $zipCode = \App\Models\Zip_code::where('zip_code', '=', $zipcodeData->d_codigo)->first();
            if (!$zipCode) { // el codigo no existe hay que generarlo

                //verificar si el municipio existe
                $municipality = \App\Models\Municipality::where('name', '=', $zipcodeData->D_mnpio)->first();
                if (!$municipality) { // el municipio no existe hay que generarlo

                    //verificar si e estado existe
                    $federalEntity = \App\Models\Federal_entity::where('name', '=', $zipcodeData->d_estado)->first();
                    if (!$federalEntity) { // el estado no existe hay que generarlo
                        $federalEntity = new \App\Models\Federal_entity();
                        $federalEntity->setAttribute('name', $zipcodeData->d_estado);
                        $federalEntity->save();

                        $this->command->info("-- Agregando zipcodes de {$federalEntity->name}");
                    }

                    $municipality = new \App\Models\Municipality();
                    $municipality->setAttribute('name', $zipcodeData->D_mnpio);
                    $municipality->federal_entity()->associate($federalEntity);
                    $municipality->save();
                }

                $zipCode = new \App\Models\Zip_code();
                $zipCode->setAttribute('zip_code', $zipcodeData->d_codigo);
                $zipCode->setAttribute('locality', $zipcodeData->d_ciudad);
                $zipCode->municipality()->associate($municipality);
                $zipCode->federal_entity()->associate($municipality->federal_entity);
                $zipCode->save();
            }

            //Agregando e settlement
            //verificar si el settlement_type existe
            $settlementType = \App\Models\Settlement_type::where('name', '=', $zipcodeData->c_tipo_asenta)->first();
            if (!$settlementType) { // el tipo de asentamiento no existe hay que generarlo
                $settlementType = new \App\Models\Settlement_type();
                $settlementType->setAttribute('name',$zipcodeData->d_tipo_asenta);
                $settlementType->save();
            }

            $settlement = new \App\Models\Settlement();
            $settlement->setAttribute('name',$zipcodeData->d_asenta);
            $settlement->setAttribute('zone_type',$zipcodeData->d_zona);
            $settlement->settlement_type()->associate($settlementType);
            $settlement->zip_code()->associate($zipCode);
            $settlement->save();

        }

    }
}

