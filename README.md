
## Xhash Reto Técnico by Rafael Reyna

Este respositorio es el desarrollo del reto técnico del proceso de aplicación par la vacante Arquitecto de Software en Xhash

El reto consiste en la implementación de un endpoint sencillo `zip-codes` a partir de una fuente de datos pública

### Cómo se abordó el problema
El principal problema en este reto, era la fuente de datos, la cual estaba en formatos de archivo no óptimos y con una estructura muy diferente ala requerida
para el endpoint del API. Con los datos estructurados de esa manera sería casi imposible servir las respuestas
en los tiempos solicitados.

La solución seleccionada para este reto fue importar totalmente la fuente de datos a una base de datos normalizada que permitiera 
tener la estructura y los indices correctos para responder de manera ópima.

#### Mapeo de la fuente de datos

Para mapear la fuente de datos a la estrucctura de tablas, se generó un seeder que utiliza el archivo descargado de la funete en formato XML, dcho seeder 
lee todos los registros de los archivos y los deposita en sus correspondientes tablas.

Este proceso, si bien es tardado por la cantida de registros, es un proceso único que permitirá tener una mejora 
sustancial en los tiempos de respuesta

#### Componentes Laravel del API
* Se realizaron migraciones para generar la estructura de la base de datos en la cual se importaría la fuente
* Se generó un seeder para hacer la migración de datos
* Se utilizaron Modelos de Eloquent, Recursos y Colecciones de Laravel y un Controlador para hacer toda la gestión de las respuestas
* Se utilizó el handler de exepciones de laravel para cachar y dar propia respuesta a los errores
* Se utilixó el middleware de throttle para limitar las peticiones por minuto, a manera de prueba se colocaron 20 peticiones
permitidas por minuto

#### Repositorio de códgo
Para este reto se realizó una pqeueña implementación de GitFlow, teniendo una rama master, una rama de desarrollo
una rama de Feature donde se realizó toda la funcionalidad, también se hizo uso de un par de ramas hotfix para arreglos menores
sobre la rama master

* NOTA: En esta ocasión hice commits más pequeños de lo habiatual, con el fn de que fuera un poco mejor la trazabilidad del repostorio
y de que se pudiera apreciar la estructura GitFlow Seleccionada

#### Documentación del proyecto
Al no haber componentes de demasiada complejidad en este reto, la mayor parte de comentarios en el código quedaron 
establecidos en el seeder, que fue el proceso más elaborado, asímismo se generó una documentación del endpoint con Postman,
y este archivo de README.me que explica algunas aprticularidades del proyecto.

#### Despliegue del proyecto
El despliegue del proyecto fue un ejercicio muy sencillo de Continuous deploy, en un Droplet del provedor Digital Ocean, 
configurado por medio de la plataforma de piloto automático runcloud.io, conectado mediante webhooks con Bitbucket, permitiendo que todos
los cambos 'mergeados' en la rama master se desplieguen en automático en el ervidor de produccción. A través de un script que instala dependencias
de Composer, y revisa que no haya migraciones pendientes por correr.
